import PyPDF2 as pypdf

#need to ask for input here
#file = 'broke_test.pdf'
print("Please type the path to (or name if in same directory) of the pdf you would like to count the number of blanks in.")
file = input()

try:
    f = open(file)
    # Do something with the file
    true_file = file
except IOError:
    #print("File not accessible")
    pass
finally:
    f.close()

if not true_file:
    try:
        f = open(file+".pdf")
        # Do something with the file
        true_file = str(file)+".pdf"
    except IOError:
        print("File not accessible")
        pass
    finally:
        f.close()

pdfobject = open(true_file ,'rb')
pdf=pypdf.PdfFileReader(pdfobject)
#print(pdf.getFormTextFields())
pdf_fields = pdf.getFormTextFields()

blank_fields = 0
for key in pdf_fields .keys():
    #print(pdf_fields[key])
    if not pdf_fields[key]: 
        blank_fields += 1	
        print(key)

print ("Blanks: " + str(blank_fields))
